/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author Samy MOKHTARI
 */
public class MySocket {
    /****** Attributes *******/
    private ServerSocket listener;
    private HashMap data;
    private Socket maSocket;
    
    /// experimental
    ObjectInputStream in;
    
    /****** Constructors ******/
    MySocket(int p_port) throws IOException {
        if(p_port > 1023){
            connect(p_port);
            in = new ObjectInputStream(maSocket.getInputStream());
        }
        else
            throw new IllegalArgumentException("Le port doit être supérieur à 1023.");
    }
    
    /****** Methods ******/
    private void connect(int p_port) throws IOException {
        listener = new ServerSocket(p_port);
        maSocket = listener.accept();
        maSocket.getInputStream();        
    }
    
    public void close() throws IOException {
        System.out.println("Fermeture de la connexion..");
        listener.close();
        maSocket.close();
        System.out.println("Le socket à été fermé.");
    }
    
    /**
     * Read and parse the data incoming by the socket and return it
     * @throws IOException if there's an error with the data format
     */
    public HashMap readData() throws IOException {
        System.out.println("Lecture des données....");
        try {
            Object result = in.readObject();
            data = (HashMap) result;
            System.out.println("Data received : " + data);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }  
        return data;
    }
}

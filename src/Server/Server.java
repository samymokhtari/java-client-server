package Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;



/**
 * API SERVER CLASS
 * @author Samy MOKHTARI
 */
public class Server {
    /****** Attributes ******/
    private Connection conn;
    private Statement myStatement;
    private String dbUrl;
    private String dbUser;
    private String dbPassword;
    private static final String TABLE = "Users";
    private static final String CREATE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS JAVA_PROJECT_LPRGI." + TABLE + "("
                    + "id INT NOT NULL,"
                    + "firstname VARCHAR(100) NOT NULL,"
                    + "lastname VARCHAR(100) NOT NULL"
                    + ")";
  

    /****** Constructors ******/
    /**
     * 
     * @param p_dbUrl
     *              URL de connexion à la base de données
     * @param p_dbUser
     *              Utilisateur de connexion à la base de données
     * @param p_dbPassword
     *              Mot de passe de connexion à la base de données
     */
    public Server(String p_dbUrl, String p_dbUser, String p_dbPassword)
    {
        this.dbUrl = p_dbUrl;
        this.dbUser = p_dbUser;
        this.dbPassword = p_dbPassword;
    }
    
    /****** Methods ******/
    
    /**
     * Chargement du driver et connexion à la base de données.
     * @throws ClassNotFoundException if class is not found.
     * @throws SQLException if we can't connect to the database.
     */
    public void connect() throws ClassNotFoundException,SQLException
    {
        Class.forName("com.mysql.cj.jdbc.Driver");
        conn =
               DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }
    /**
     * Close database connection.
     * @throws SQLException if we can't close the database connection
     */
    public void close() throws SQLException {
        myStatement.close();
        conn.close();
    }
    /**
     * Initialize database by creating the Users table
     * @throws SQLException if we can't create table (no permissions).
     */
    public void initialize() throws SQLException{
        myStatement = conn.createStatement();
        myStatement.execute(CREATE_TABLE_SQL);
    }
    
    /**
     * Insert Data into Users table
     * @param p_firstname user firstname
     * @param p_lastname user lastname
     * @throws SQLException if there is an error when we attempt to insert data into users.
     */
    public void insert(String p_firstname, String p_lastname) throws SQLException, JSONException{
        final String SQL_INSERT = 
                ("INSERT INTO " + TABLE + "(id, firstname, lastname) VALUES ("+ myStatement.getMaxRows() +",'"+ p_firstname +"', '"+p_lastname+"')");
        myStatement.executeUpdate(SQL_INSERT);
        
    }
    
    /**
     * Get all users from database. (Not used but can be useful for future)
     * @return JSON array for all the values fetched.
     * @throws SQLException if we can't fetch users.
     * @throws JSONException if we can't convert data into a JSON format.
     */
    public JSONArray fetch() throws SQLException, JSONException{
        String SQL_SELECT = "SELECT firstname,lastname FROM " + TABLE;
        ResultSet result = myStatement.executeQuery(SQL_SELECT);
        JSONArray json = new JSONArray();
        
        while(result.next()){
            JSONObject obj=new JSONObject();
            obj.put("firstname",result.getString("firstname"));
            obj.put("lastname",result.getString("lastname"));
            json.put(obj);
        }
        
        return json;
    }
}
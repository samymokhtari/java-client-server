/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import org.json.JSONException;
/**
 *
 * @author Samy MOKHTARI
 */
public class TestServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // VARIABLES
        final String DB_URL = "jdbc:mysql://5kage.xyz/JAVA_PROJECT_LPRGI";
        final String DB_USER = "java_project";
        final String DB_PASSWORD = "java_password";
        final int PORT = 9999;
        MySocket socket = null;
        Server api = null;
        HashMap data = null;
        boolean flag = false;
        
        /* Connexion au socket */
        try{
            System.out.println("En attente de connexion..");
            socket = new MySocket(PORT);
            System.out.println("Connexion au socket OK");
        }catch(IOException ex){
            ex.printStackTrace();
        }

        
        /* Initialize the connection on the database */
        try {
            api = new Server(DB_URL, DB_USER, DB_PASSWORD);
            api.connect();
            api.initialize(); /* Initialize "User" table if it does not exists */
            System.out.println("Chargement du driver JDBC : Succes ! "
                    + "\n Connexion à la base de données OK");
            
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            System.exit(-1);
        }
        catch (ClassNotFoundException ex) {
            System.err.println("Echec au chargement du driver JDBC.");
            System.exit(-2);
        }

        /* GET DATA AND INSERT - WHILE SIGNAL IS NOT 'STOP' */
        while(flag == false){
                
            try{
                data = socket.readData();
                if(data.containsKey("signal") && data.get("signal").toString().equals("stop")) {
                    flag = true;
                }else {
                    String firstname = data.get("firstname").toString();
                    String lastname = data.get("lastname").toString();
                    api.insert(firstname, lastname);
                    System.out.println("L'insertion de l'utilisateur : "+ firstname + " "+lastname+" à bien été prise en compte.");
                    System.out.println("\nContenu de la base :\n"+api.fetch()); 
                }
                

            }catch(SQLException ex){
                System.out.println("L'insertion à échoué: "+ex.getMessage());
            }
            catch(JSONException ex){
                System.out.println("L'insertion à échoué: "+ex.getMessage());
            }
            catch(IOException ex){
                System.out.println("La lecture des données à échoué: "+ex.getMessage());
            }
        }
        /* Fermeture de la connexion base de données */
        try {
            socket.close();
            api.close();
        } catch (SQLException ex) {
            System.out.println("La fermeture n'a pas pu s'effectuer : " + ex.getMessage());
        }catch(IOException ex){
                System.out.println("La fermeture du socket à échoué: "+ex.getMessage());
            }
        
    }
    
}

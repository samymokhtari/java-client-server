/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author Yanis LECLERCQ
 */
public class Panneau extends javax.swing.JPanel {
    private ObjectOutputStream out = null;
    private Socket maSocket = null;
    private final int NUM_PORT = 9999;
    private final String HOSTNAME = "localhost";
    /**
     * Creates new form Panneau
     */
    public Panneau() {
        initComponents();
        setBackground(Color.lightGray);
        setPreferredSize( new Dimension( 840, 240 ) );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_nom = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        Saisie_nom = new javax.swing.JTextPane();
        lbl_prenom = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Saisie_prenom = new javax.swing.JTextPane();
        btn_valider = new javax.swing.JButton();
        btn_connect = new javax.swing.JButton();
        btn_disconnect = new javax.swing.JButton();
        lbl_saisie = new javax.swing.JLabel();

        setLayout(new java.awt.GridLayout(4, 2));

        lbl_nom.setText("Nom");
        add(lbl_nom);

        jScrollPane2.setViewportView(Saisie_nom);

        add(jScrollPane2);

        lbl_prenom.setText("Prenom");
        add(lbl_prenom);

        jScrollPane1.setViewportView(Saisie_prenom);

        add(jScrollPane1);

        btn_valider.setText("Valider");
        btn_valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_validerActionPerformed(evt);
            }
        });
        add(btn_valider);

        btn_connect.setText("Connexion à l'API");
        btn_connect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_connectActionPerformed(evt);
            }
        });
        add(btn_connect);

        btn_disconnect.setText("Déconnexion de l'API");
        btn_disconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_disconnectActionPerformed(evt);
            }
        });
        add(btn_disconnect);

        lbl_saisie.setText("Informations saisie..");
        add(lbl_saisie);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Send data to the server by writing on the output
     * @param evt 
     */
    private void btn_validerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_validerActionPerformed
        if(maSocket == null){
            lbl_saisie.setText("Vous n'êtes pas connecté");
            return;
        }
        if (Saisie_prenom.getText().isEmpty() || (Saisie_nom.getText().isEmpty()) ){
            lbl_saisie.setText("Les données à envoyer ne peuvent pas être vide !");
            return;
        }
        
        if (!maSocket.isClosed()) {
            HashMap<String, String> dataToSend = new HashMap<String, String>();
            try {
                dataToSend.put("firstname", Saisie_prenom.getText());
                dataToSend.put("lastname", Saisie_nom.getText());
                out.writeObject(dataToSend);
                out.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-3);
            }
            lbl_saisie.setText("Les données ont bien été envoyés.");
        } else {
            lbl_saisie.setText("Vous devez être connecté pour effectuer cette action.");
        }
        
    }//GEN-LAST:event_btn_validerActionPerformed

    /**
     * Connect to the socker
     * @param evt 
     */
    private void btn_connectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_connectActionPerformed
        try {  
            maSocket = new Socket(HOSTNAME, NUM_PORT);
            out = new ObjectOutputStream(maSocket.getOutputStream());
            out.flush();
            lbl_saisie.setText("Vous êtes connecté");
        } catch (IOException ex) {
            lbl_saisie.setText("La connexion au serveur distant "
                +HOSTNAME+":"+NUM_PORT+" est impossible");
        }
    }//GEN-LAST:event_btn_connectActionPerformed

    /**
     * Disconnect the client to the server and close the API by sending 'STOP'
     * @param evt 
     */
    private void btn_disconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_disconnectActionPerformed
        if (maSocket != null) {
            if(!maSocket.isClosed()){
                HashMap<String, String> dataToSend = new HashMap<String, String>();
                try {
                    dataToSend.put("signal", "stop");
                    out.writeObject(dataToSend);
                    out.flush();
                } catch (IOException ex) {
                    System.out.println("Erreur deconnexion "+ex.getMessage()); 
                    System.exit(-4);
                }
                try {
                    out.close();
                    maSocket.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage()); 
                    System.exit(-5);
                }
                finally {
                    lbl_saisie.setText("Le client à bien été déconnecter de l'API et celle-ci à été fermée.");
                }
            } 
        }
    }//GEN-LAST:event_btn_disconnectActionPerformed
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane Saisie_nom;
    private javax.swing.JTextPane Saisie_prenom;
    private javax.swing.JButton btn_connect;
    private javax.swing.JButton btn_disconnect;
    private javax.swing.JButton btn_valider;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbl_nom;
    private javax.swing.JLabel lbl_prenom;
    private javax.swing.JLabel lbl_saisie;
    // End of variables declaration//GEN-END:variables
}
